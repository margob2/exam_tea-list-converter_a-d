Se lab_tea-list-converter_beskrivning.pdf för laborations beskrivning.

Application: TeaListFileConverter
Version: 0.3

The applications lets you convert files with tea information between file formats and files.

The application is console based and requires two parameters provided when the program is started:

[file name read][file name write]

Supported file formats in version 0.3

Read:
csv

Write:
dsv

Example of valid parameters

tealist.csv tealist.dsv
(converts the csv-file tealist.csv to dsv format in file tealist.dsv)

