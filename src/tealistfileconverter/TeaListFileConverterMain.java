package tealistfileconverter;

/**
 * Program entry point. See readme file for 
 * more information.
 * 
 * @author Thomas Ejnefjäll
 * @author Martin Goblirsch
 */
public class TeaListFileConverterMain {

	/**
	 * Program entry point. The provided options (String[]) must contain two values: 
	 * [0] name of the file we want to read from, [1] name of the file we want to create and copy the tea information 
	 * to. 
	 * Both files need to specify file type (EX: .csv / .dsv)
	 * 
	 * @param options Program parameters. 
	 */
	public static void main(String[] options) {
		try {

			TeaListFileConverter tlfc = new TeaListFileConverter();
			tlfc.processRequest(options);

			System.out.println("Conversion complete");
		} catch (Exception e) {			
			System.out.println(e.getMessage());
		}
	}
}