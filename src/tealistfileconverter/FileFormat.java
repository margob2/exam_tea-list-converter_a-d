package tealistfileconverter;

/**
 * Enumeration for supported file formats.
 * 
 * @author Thomas Ejnefjäll
 * @author Martin Goblirsch
 */
public enum FileFormat {
	CSV("csv"), DSV("dsv"), XML("xml");

	private String fileFormat;

	/**
	 * Private constructor only for the enumeration itself.
	 * 
	 * @param fileFormat Name of the file format.
	 */
	private FileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	/**
	 * Check if the current file format is equal to another.
	 * 
	 * @param fileFormat A string containing the other file format.
	 * @return true it they are the same file format.
	 */
	public boolean equals(String fileFormat) {
		return this.fileFormat.equalsIgnoreCase(fileFormat);
	}

	/**
	 * Check if the current file format is equal to another.
	 * 
	 * @param fileFormat The other file format.
	 * @return true it they are the same file format.
	 */
	public boolean equals(FileFormat fileFormat) {
		return this.equals(fileFormat.fileFormat);
	}

	/**
	 * Check if the string contains a file format that is valid to transform into(part of the
	 * enumeration).
	 * 
	 * @param fileFormat A string containing the file format.
	 * @return true If the file format is valid (part of the enumeration).
	 */
	public static boolean isValidTo(String fileFormat) {

		if (DSV.equals(fileFormat)) {
			return true;
		} else if (XML.equals(fileFormat)) {
			return true;
		}

		return false;
	}

	/**
	 * Check if the string contains a file format that is valid to transform from (part of the
	 * enumeration).
	 * 
	 * @param fileFormat A string containing the file format.
	 * @return true If the file format is valid (part of the enumeration).
	 */
	public static boolean isValidFrom(String fileFormat) {

		if (CSV.equals(fileFormat)) {
			return true;
		} 

		return false;
	}


	/**
	 * Return a String with valid file formats.
	 * 
	 * @return A String with valid file formats.
	 */
	public static String validFormats() {
		String validFormats = "";

		for (FileFormat f : FileFormat.values()) {
			validFormats += f.toString() + " ";
		}
		return validFormats;
	}

	@Override
	public String toString() {
		return this.fileFormat;
	}
}