package tealistfileconverter;

import java.util.List;

import tealistfileconverter.converters.TeaListConverter;
import tealistfileconverter.converters.TeaListConverterCsv;
import tealistfileconverter.converters.TeaListConverterDsv;
import tealistfileconverter.converters.TeaListConverterXml;

/**
 * Handles parsing of options and conversion between different files 
 * containing information about tea.
 * 
 * @author Thomas Ejnefjäll
 * @author Martin Goblirsch
 */
public class TeaListFileConverter 
{	
	
	/**
	 * Process a tea list file converter request. The provided options (String[])
	 * must contain four values: [0] name of the file we want to read from,
	 * [1] file format of the file we want to read from, [2] name of the 
	 * file we want to create and copy the tea information to, and
	 * [3] the file format of the file we want to create.  
	 * 
	 * @param options The four options.
	 * @throws Exception If there is an error while converting the file. 
	 */
	public void processRequest(String[] options) throws Exception  {
		if(options.length != 2) {
			throw new IllegalArgumentException("Two parameters must be provided: FileFrom.FormatFrom FileTo.FormatTo");
		}
		convertFile(options[0], options[0].substring(options[0].lastIndexOf(".") + 1), options[1], options[1].substring(options[1].lastIndexOf(".") + 1));
	}
	
	/**
	 * Convert a file containing tea information from one format to another.
	 *  
	 * @param fileFrom The source file containing the tea information.
	 * @param formatFrom The format of the source file.
	 * @param fileTo The destination file for the tea information.
	 * @param formatTo The format of the destination file. 
	 * @throws Exception If there is an error while converting the file. 
	 */
	private void convertFile(String fileFrom, String formatFrom, String fileTo, String formatTo) throws Exception {
		
		if(!FileFormat.isValidFrom(formatFrom) || !FileFormat.isValidTo(formatTo)) {
			throw new IllegalArgumentException("Format not supported. Valid formats: " + FileFormat.validFormats());
		}	

		TeaListConverter tlc = null;
		List<Tea> teaList = null;
		
		//We always convert from CSV format.
		tlc = new TeaListConverterCsv();
		//Read information from the CSV file  
		teaList = tlc.convert(fileFrom);
		

		//Currently we always convert to DSV format
		tlc = new TeaListConverterDsv();
		//We use the DSV converter to do the actual conversion 
		tlc.convert(teaList, fileTo);
	}	
}